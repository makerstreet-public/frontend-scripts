#!/usr/bin/env node

'use strict'

const { version } = require('../package.json')
const yargs = require('yargs')
const { runCommand } = require('../utils/helpers')

console.log(`frontend-scripts ${version}`)

const args = yargs
  .demandCommand(1)
  .usage('$0 <cmd> [args]')
  .command(
    'build', 'build source',
    (yargs) => {
      return yargs
        .alias('e', 'environment')
        .describe('e', 'specify an environment')
        .default('e', process.env.NODE_ENV || 'development')
        .alias('t', 'task')
        .describe('t', 'specify a task')
        .default('t', '*', 'all')
        .help('h')
        .alias('h', 'help')
    },
    (argv) => {
      console.log(`Building for environment: ${argv.environment}`)
      runCommand('build', argv.task, argv.environment)
    }
  )
  .command(
    'watch', 'watch source for development',
    (yargs) => {
      return yargs
        .alias('e', 'environment')
        .describe('e', 'specify an environment')
        .default('e', process.env.NODE_ENV || 'development')
        .alias('t', 'task')
        .describe('t', 'specify a task')
        .default('t', '*', 'all')
        .help('h')
        .alias('h', 'help')
    },
    (argv) => {
      console.log(`Watching for environment: ${argv.environment}`)
      runCommand('watch', argv.task, argv.environment)
    }
  )
  .command(
    'release', 'make a release build',
    (yargs) => {
      return yargs
        .alias('t', 'task')
        .describe('t', 'specify a task')
        .default('t', '*', 'all')
        .help('h')
        .alias('h', 'help')
    },
    (argv) => {
      console.log('Creating a release build')
      runCommand('build', argv.task, 'production')
    }
  )
  .help()
  .argv



