const paths = require('../config/paths')
const appConfig = require('../config/app')
const logger = require('../utils/logger').create('system')
const path = require('path')
const fs = require('fs')
const deepmerge = require('deepmerge')


const childProcess = require('child_process')

function getEnvironment() {
  return process.env.NODE_ENV || 'development'
}

function getConfig (taskName, config = {}, environment = getEnvironment()) {
  const filePaths = [
    path.resolve(paths.taskConfigs, `${taskName}.js`),
    path.resolve(paths.taskConfigs, `${taskName}.${environment}.js`)
  ]

  let output = filePaths
    .filter(path => fs.existsSync(path))
    .reduce((config, path) => deepmerge(config, require(path)), {})

  return deepmerge(output, config);
}

const taskExists = (task) => {
  try {
    require.resolve(path.join(paths.tasks, `${task}`))
    return true;
  } catch (e) {
    return false
  }
}

const runCommand = (command, taskName, environment) => {

  let tasks = appConfig.tasks
    .map(task => Array.isArray(task) ? task[0] : task)
    .filter(name => (taskName === '*' || taskName === name))

  tasks = Array.from(new Set(tasks))
  let missingTasks = tasks.filter(task => !taskExists(task))
  tasks = tasks.filter(taskExists)
  logger.info('Frontend Scripts')
  logger.info(`Running command: {command:${command}}`)
  logger.info('    Environment: {command:%s}', getEnvironment())
  logger.info(`          Tasks: {task:${tasks.join(', ')}}`)
  if (missingTasks.length) {
    logger.warn(`  Missing tasks: {task:${missingTasks.join(', ')}}`)

  }

  return Promise.all(tasks.map(task => runTask(command, task, environment)))
    .then(results => {
      logger.info(`{size:${results.length}} tasks executed`)
    })
    .catch(errors => {
      logger.error(`{size:${results.length}} tasks errored`)
    })
}

const runTask = async (command, task, environment) => {

  return new Promise((resolve, reject) => {
    const scriptPath = path.resolve(paths.scripts, 'run');
    const env = Object.assign({}, process.env, {
      NODE_ENV: environment,
    })
    const subprocess = childProcess.spawn(
      'node',
      [ scriptPath, command, task ],
      { stdio: 'inherit', env }
    )

    subprocess.on('exit', (code, signal) => {
      if(signal === 'SIGKILL') {
        return reject(`${command}:${task} failed. This probably means the system ran out of memory or someone called \`kill -9\` on the process`)
      } else if(signal === 'SIGTERM') {
        return reject(`${command}:${task} failed. Someone might have called \`kill\` or \`killall\`, or the system could be shutting down`)
      }
      resolve();
    })
  })
}

const ensureArray = (val) => Array.isArray(val) ? val : [val]
const falseOrObj = (val) => (val === true ? {} : val) || undefined

const fixChokidarPath = (sourcePath) => path.sep === '\\' ? sourcePath.replace(/\\/g, '/') : sourcePath

module.exports = {
  getConfig,
  getEnvironment,
  runCommand,
  ensureArray,
  falseOrObj,
  taskExists,
  fixChokidarPath
}
