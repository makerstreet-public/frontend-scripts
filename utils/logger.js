const tFunk = require('tfunk').Compiler({
  file: function(string) { return this.compile(`{yellow:${escape(string)}}`) },
  task: function(string) { return this.compile(`{green:${escape(string)}}`) },
  size: function(string) { return this.compile(`{magenta:${escape(string)}}`) },
  command: function(string) { return this.compile(`{green:${escape(string)}}`) },
  var: function(string) { return this.compile(`{green:${escape(string)}}`) },
})
const util = require('util')

let colors = {
  javascript: 'blue',
  css: 'green',
  html: 'pink',
  icons: 'magenta',
  system: 'white'
}

const levels = {
  "trace": "[trace]",
  "debug": "{magenta:[debug]}",
  "info":  " {cyan:[info]}",
  "warn":  " {yellow:[warn]}",
  "error": "{red:[error]}",
  "deprecate": "{orange:[deprecate]}",
}

const createLogLevel = (nsPrefix, lvlPrefix) => (string, ...args) => {
  let msg = util.format(`${lvlPrefix} ${nsPrefix} ${string}`, ...args)
  console.log(tFunk.compile(msg))
}

const createLogger = (namespace, thread) => {

  const nsColor = colors[namespace] || 'green'
  const nsThread = thread !== undefined ? `{white::}{${nsColor}:${thread}}`:''
  const nsPrefix = `{white:[}{${nsColor}:${namespace}}${nsThread}{white:]}`

  return Object.entries(levels).reduce((logger, [fn, lvlPrefix]) => {
    logger[fn] = createLogLevel(nsPrefix, lvlPrefix, thread);
    return logger
  }, {escape: escape})
}

const escape = (string) => string.replace(/\{/g,'\\{').replace(/\}/g,'\\}')

module.exports = {
  create : createLogger,
  escape: escape
}
