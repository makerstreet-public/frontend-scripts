const {codeFrameColumns} = require('@babel/code-frame')
const fs = require('fs')
const logger = require('./logger')

class BuilderBase {
  constructor(task, config, thread) {
    this.taskName = task;
    this.config = {...config};
    this.logger = logger.create(task, thread);
    Object.assign(this,this.logger);
  }

  async build() {
    this.error('Build script is not implemented')
  }
  async watch() {
    this.error('Watch script is not implemented')
  }
  async release() {
    this.error('Release script is not implemented')
  }


  handleErrors (error) {
    if (!error.codeFrame) {
      if (error.file && error.line && error.column) {
        error.codeFrame = codeFrameColumns(
          fs.readFileSync(error.file, {encoding: 'utf-8'}),
          {
            start: {
              line: error.line,
              column: error.column
            }
          }
        )
      }
    }

    if (error.codeFrame) {
      this.error("%s \r\n%s",
        logger.escape(error.messageOriginal || error.message),
        logger.escape(error.codeFrame)
      )
    } else if (error.file && error.line && error.column && error.message) {
      this.error(Logger.escape(error.messageFormatted))
    } else {
      this.error(error)
    }
  }
}

module.exports = BuilderBase
