const map = require('map-stream')
const through = require('through2')
const noop = () => through.obj()
const path = require('path');
const getFileSize = require('filesize')
const fs = require('fs')

const fileInfo = (cb) => {

  return map((file, done) => {
    let data = {
      name: path.basename(file.path),
      size: file.stat ? getFileSize(file.stat.size) : getFileSize(Buffer.byteLength(String(file.contents)))
    }
    if (file.history && file.history.length && fs.existsSync(file.history[0])) {
      data = {
        ...data,
        sourcePath: file.history[0],
        sourceSize: getFileSize(fs.statSync(file.history[0]).size)
      };
    }
    cb(data)
    done(null, file)
  })
}

const pipeIf = (condition, cb) => {
  return condition ? cb() : through.obj()
}



module.exports = {
  fileInfo,
  noop,
  pipeIf
}