const { getConfig, taskExists } = require('../utils/helpers')
const appConfig = require('../config/app')
const logger = require('../utils/logger').create('system')
const paths = require('../config/paths')
const path = require('path')

const argv = require('yargs')
  .usage('Usage: $0 <command> <task>')
  .command('command')
  .command('task')
  .required(2, 'both command and task are required')
  .help()
  .argv

const COMMAND = argv._[0]
const TASK_NAME = argv._[1]
const NODE_ENV = process.env.NODE_ENV || 'develop'

if (!taskExists(TASK_NAME)) {
  logger.error(`Task {task:${TASK_NAME}} is specified but has no scriptfile`)
  process.exit(0)
}

let TaskBuilder = require(path.resolve(paths.tasks, TASK_NAME))

if (!appConfig.tasks) {
  logger.error(`{file:package.json} has no tasks specified`)
  process.exit(0)
}

let tasks = appConfig.tasks
  .map(task => Array.isArray(task) ? task : [task])
  .filter(task => task[0] === TASK_NAME)

if (!tasks.length) {
  logger.error(`{file:package.json} has no tasks specified for {task:${TASK_NAME}}`)
  process.exit(0)
}

try {
  // yes... hacky, but it works so
  let gutils = require('gulp-util')
  gutils.log = () => {}
} catch (e) {}

tasks = tasks
  .map(([taskName, taskConfig], idx) => {
    let config = getConfig(taskName, taskConfig, NODE_ENV)
    let builder = new TaskBuilder(TASK_NAME, config, (tasks.length > 1) ? idx + 1 : undefined)
    return builder[COMMAND]().catch(error => builder.error(error))
  })
