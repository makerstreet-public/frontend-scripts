# Installation

## Adding the package
```
npm install @touchtribe/frontend-scripts
```

## Adding the CLI commands

In `package.json`:
```
{
  ...,
  "scripts": {
    "build": "frontend-scripts build",
    "watch": "frontend-scripts watch",
    "release": "frontend-scripts release",
  },
  ...
}
```

## Adding tasks 

`frontend-scripts` reads tasks out of the `package.json` file:
```
{
  ...,
  "frontend": {
    "sourcePath": "./src",
    "buildPath": "./dist",
    "tasks": []
  },
  ...
}
```

## Environment specific tasks

To have environment specific build tasks, add them according to the index `frontend.<environment>`:
```
{
  ...,
  "frontend": {
    "sourcePath": "./src"
  },
  "frontend.development": {
    "buildPath": "./dist",
    "tasks": []
  },
  "frontend.production": {
    "buildPath": "./release",
    "tasks": []
  },
  ...
}
```
The resulting configuration is a merge of:
*  Base task config
*  `frontend`
*  `frontend.environment` (if it exists)

# Tasks

Tasks get added using the following signature:
```
// taskname: string
// config: configuration object, will deep-merge the base config
[<taskname>, <config>]
```
Environment-specific tasks do not merge with global tasks but are added as separate tasks.

## Task configuration

*  [javascript](https://gitlab.techtribe.nl/touchtribe-public/frontend-scripts/wikis/Tasks:-javascript)
*  [css](https://gitlab.techtribe.nl/touchtribe-public/frontend-scripts/wikis/Tasks:-css)
*  copy
*  handlebars
*  browsersync
*  images
*  icons


### Todo

-  [x] Publish on NPM ;-)
-  [ ] Migrate documentation
-  [ ] Complete documentation
-  [ ] Rewrite task runner to single process
-  [ ] Move tasks to seperate modules
