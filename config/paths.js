const appConfig = require('./app')
const path = require('path');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const scriptsDirectory = path.resolve(fs.realpathSync(__dirname), '..');
const resolveScripts = relativePath => path.resolve(scriptsDirectory, relativePath);

module.exports = {
  dotenv: resolveApp('.env'),
  appDirectory: appDirectory,
  appPackage: resolveApp('package.json'),
  appModules: resolveApp('node_modules'),
  appSource: resolveApp(appConfig.sourcePath || 'src'),
  appBuild: resolveApp(appConfig.buildPath || 'dist'),

  scripts: resolveScripts('scripts'),
  tasks: resolveScripts('tasks'),
  config: resolveScripts('config'),
  taskConfigs: resolveScripts('config/tasks')
}
