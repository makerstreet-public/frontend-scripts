const cache = {};
const packageCache = {}

module.exports = {
  browserify: {
    debug: true,
    cache: cache,
    packageCache: packageCache,
    fullpaths: true,
    paths: [
      'node_modules'
    ],
    transform: {
      "babelify": {}
    }
  },
  sourcemaps: {
    loadMaps: true
  }
}
