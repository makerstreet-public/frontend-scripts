module.exports = {
  browserify: {
    fullpaths: false,
    cache: undefined,
    packageCache: undefined,
    debug: false,
    transform: {
      "envify": {},
      "uglifyify": {
        global: false
      }
    }
  },
  minify: true,
  sourcemaps: false
}
