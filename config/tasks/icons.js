module.exports = {
  types: ['eot', 'woff', 'woff2', 'ttf', 'svg'],
  fontname: 'icon-font',
  className: 'icon',
  sass: {
    template: './sass.hbs',
    asSource: true
  }
}