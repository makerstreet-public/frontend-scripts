const deepmerge = require('deepmerge')
const path = require('path')

const getAppConfig = (pkg, key = 'frontend', environment = (process.env.NODE_ENV || 'development')) => {
  if (typeof(pkg) === 'string') {
    pkg = require(pkg)
  }
  return [
    `${key}`,
    `${key}.${environment}`
  ].reduce((config, path) => {
    if (pkg[path]) {
      config = deepmerge(config, pkg[path])
    }
    return config;
  }, {})
}
module.exports = getAppConfig(path.join(process.cwd(), './package.json'))
