const BuilderBase = require('../utils/builder-base');

const browserify = require('browserify');
const source = require('vinyl-source-stream')
const {appSource, appBuild} = require('../config/paths');
const {ensureArray, falseOrObj} = require('../utils/helpers');
const {fileInfo, pipeIf} = require('../utils/stream')
const vinylFs = require('vinyl-fs');
const buffer = require('vinyl-buffer');
const path = require('path');
const watchify = require('watchify')
const rename = require('gulp-rename')

const terser = require('gulp-terser')
const sourcemaps = require('gulp-sourcemaps')

class JavascriptBuilder extends BuilderBase {

  init() {
    let config = this.config;

    if (!config.source) {
      throw new Error('{var:javascript.source} must be a file or an array of files')
    }
    if (!config.dest) {
      throw new Error('{var:javascript.dest} must be a output directory')
    }
    if (!config.filename) {
      throw new Error('{var:javascript.filename} must be the name of the output file')
    }

    this.config = {
      ...config,
      source: ensureArray(config.source).map(entry => path.resolve(appSource, entry)),
      browserify: {...config.browserify, transform: Object.entries(config.browserify.transform || {})},
      dest: path.resolve(appBuild, config.dest)
    }
  }

  async build (hasWatch = false) {
    this.init()
    let bundler = this.createBundler()
    if (hasWatch === true) {
      bundler = watchify(bundler)
      bundler.on('update', () => this.runBundle(bundler))
    }
    return this.runBundle(bundler)
  }
  async watch() {
    return this.build(true)
  }

  createBundler () {
    const {source, browserify: config} = this.config;
    return browserify(source, config)
  }

  runBundle (bundler) {
    const config = this.config;
    this.info('Building: {file:%s}', config.source.map(entry => path.relative(process.cwd(), entry)).join(', '))
    return bundler
      .bundle()
      .on('error', this.handleErrors.bind(this))
      .pipe(source(path.basename(config.filename)))
      .pipe(buffer())
      .pipe(rename({dirname: ''}))
      .pipe(pipeIf(config.sourcemaps, () => sourcemaps.init(config.sourcemaps)))
      .pipe(pipeIf('minify' in config && config.minify, () => terser(falseOrObj(config.minify))))
      .pipe(pipeIf(config.sourcemaps, () => sourcemaps.write('./')))
      .pipe(vinylFs.dest(config.dest))
      .pipe(fileInfo(file => {
        this.info('    Done: {file:%s} [{size: %s}]',
          path.relative(process.cwd(), path.resolve(config.dest, file.name)),
          file.size
        )
      }))
  }
}



module.exports = JavascriptBuilder
