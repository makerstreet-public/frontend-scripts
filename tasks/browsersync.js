const BuilderBase = require('../utils/builder-base')
const {appBuild} = require('../config/paths')
const {ensureArray} = require('../utils/helpers')

const path = require('path')

const BrowserSync = require('browser-sync')

class BrowserSyncBuilder extends BuilderBase {

  init() {
    let config = this.config

    if (!config.basePath) {
      throw new Error('{var:config.basePath} must be defined')
    }

    if (!config.files) {
      throw new Error('{var:config.files} must be defined')
    }

    if (config.single) {
      config.single = path.resolve(appBuild, config.single)
    }

    this.config = {
      ...config,
      files: ensureArray(config.files).map(entry => path.resolve(appBuild, entry)),
      basePath: path.resolve(appBuild, config.basePath)
    }

    this.bs = BrowserSync.create()

    this.init = () => {
    }
  }

  async build() {

  }

  async watch() {
    this.init()

    const {
      basePath,
      files,
      port,
      https,
      proxy,
      single,
      serveStatic = ['.']
    } = this.config

    let options = {
      files,
      port,
      https
    }

    if (proxy)
      Object.assign(options, {proxy: proxy, serveStatic})
    else
      Object.assign(options, {server: basePath, single})

    this.bs.init(options)
  }
}


module.exports = BrowserSyncBuilder
