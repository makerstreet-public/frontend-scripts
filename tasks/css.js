const BuilderBase = require('../utils/builder-base');
const {appSource, appBuild} = require('../config/paths');
const {ensureArray} = require('../utils/helpers');
const {fileInfo, pipeIf} = require('../utils/stream')
const {fixChokidarPath} = require('../utils/helpers')

const vinylFs = require('vinyl-fs');
const path = require('path');
const rename = require('gulp-rename')
const sourcemaps = require('gulp-sourcemaps')
const postcss = require('gulp-postcss')
const cssnano = require('cssnano')
const autoprefixer = require('autoprefixer')
const sass = require('gulp-sass')
const less = require('gulp-less')

const plumber = require('gulp-plumber')
const chokidar = require('chokidar')
const throttle = require('lodash.throttle')

const {falseOrObj} = require('../utils/helpers')

class CssBuilder extends BuilderBase {

  init() {
    let config = this.config;

    if (!config.source) {
      throw new Error('{var:css.source} must be a path to the main file')
    }
    if (!config.dest) {
      throw new Error('{var:css.dest} must be a output directory')
    }
    if (!config.filename) {
      throw new Error('{var:css.filename} must be a filename (Output css filename)')
    }

    this.config = {
      ...config,
      source: path.resolve(appSource, config.source),
      dest: path.resolve(appBuild, config.dest)
    }
    this.init = () => {};
  }
  async build () {
    this.init()
    let config = this.config;
    const self = this;

    let postCssPlugins = []
    if (config.autoprefixer) {
      postCssPlugins.push(autoprefixer(falseOrObj(config.autoprefixer)))
    }
    if (config.minify) {
      postCssPlugins.push(cssnano(falseOrObj(config.minify)))
    }

    this.info('Building: {file:%s}', path.relative(process.cwd(), config.source))
    return vinylFs.src(config.source)
      .pipe(plumber({
        errorHandler: function(error) {
          self.handleErrors(error)
          this.emit('end')
        }
      }))
      .pipe(rename({dirname: ''}))
      .pipe(pipeIf(config.sourcemaps, () => sourcemaps.init(falseOrObj(config.sourcemaps))))
      .pipe(pipeIf(config.sass, () => sass(falseOrObj(config.sass))))
      .pipe(pipeIf(config.less, () => less(falseOrObj(config.less))))
      .pipe(pipeIf(postCssPlugins.length, () => postcss(postCssPlugins)))
      .pipe(rename(config.filename))
      .pipe(pipeIf(config.sourcemaps, () => sourcemaps.write('./')))
      .pipe(vinylFs.dest(config.dest))
      .pipe(fileInfo(file => {
        this.info('    Done: {file:%s} [{size: %s}]',
          path.relative(process.cwd(), path.resolve(config.dest, file.name)),
          file.size
        )
      }))
  }
  async watch() {
    this.init();
    let extensions = []
    if (this.config.sass) {
      extensions.push('sass','scss')
    }
    if (this.config.less) {
      extensions.push('less')
    }
    extensions = extensions.length === 1 ? extensions[0] : `{${extensions.join(',')}}`
    let source = path.dirname(this.config.source) + `/**/*.${extensions}`
    let relativeSource = this.logger.escape(path.relative(process.cwd(), source))
    this.watcher = chokidar.watch(fixChokidarPath(source))
    this.watcher
      .on('error', (error) => this.handleErrors(error))
      .on('ready', () => {
        this.info(`Watching {file:${relativeSource}}`)
        this.build()
        this.watcher.on('all', throttle(() => this.build(), 200))
      })
  }
}



module.exports = CssBuilder
