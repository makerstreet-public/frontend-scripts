const BuilderBase = require('../../utils/builder-base')
const { appSource, appBuild, appDirectory } = require('../../config/paths')

const WebfontGenerator = require('@vusion/webfonts-generator')
const glob = require('glob')
const path = require('path')
const chokidar = require('chokidar')
const getFileSize = require('filesize')
const throttle = require('lodash.throttle')

class IconsBuilder extends BuilderBase {

  init () {
    const config = this.config
    if (!config.source) {
      throw new Error('{var:icons.source} must be a glob of the source directory')
    }
    if (!config.dest) {
      throw new Error('{var:icons.dest} must be a output directory')
    }
    if (!config.fontname) {
      throw new Error('{var:icons.fontname} must be a string containing a name')
    }

    this.config.source = path.join(appSource, this.config.source)
    this.config.dest = path.join(appBuild, this.config.dest)

    if (config.sass) {
      if (!config.sass.dest) {
        throw new Error('{var:icons.sass.dest} must be a path to the sass output directory')
      }
      if (!config.sass.filename) {
        throw new Error('{var:icons.sass.filename} must be a valid filename')
      }
      if (!config.sass.template) {
        throw new Error('{var:icons.sass.template} must be a path to the sass template file')
      }
      let baseDir = config.sass.asSource ? appSource : appBuild
      config.sass.dest = path.join(baseDir, config.sass.dest, config.sass.filename)
      let tplPath
      try {
        tplPath = require.resolve(config.sass.template)
      } catch (e) {
        try {
          tplPath = require.resolve(path.join(appSource, config.sass.template))
        } catch (e) {
          throw new Error('{var:icons.sass.template} must be a path to the sass template file')
        }
      }
      config.sass.template = tplPath
    }
    this.init = () => {
    }
  }

  async build () {
    this.init()
    let config = this.config

    let conf = {
      files: glob.sync(path.relative(appDirectory, config.source)),
      dest: path.relative(appDirectory, config.dest),
      fontName: config.fontname,
      css: false,
      types: config.types,
      templateOptions: {
        className: config.className
      },
      fontHeight: 1001,
      html: false,

      // defaults to make the hash for the generated css work properly
      htmlTemplate: '',
      cssTemplate: '',
      cssDest: '',
      cssFontsUrl: '',
      htmlDest: ''
    }
    if (config.sass) {
      conf = {
        ...conf,
        css: true,
        cssDest: path.relative(appDirectory, config.sass.dest),
        cssTemplate: path.relative(appDirectory,config.sass.template),
        cssFontsUrl: path.relative(appDirectory, config.sass.fontPath)
      }
    }

    return new Promise((resolve, reject) => {
      WebfontGenerator(conf, (error, result) => {
        if (error) {
          return reject(error)
        }
        config.types.forEach(type => {
          let content = result[type]
          this.info('    Done: {file:%s} [{size: %s}]',
            path.relative(process.cwd(), path.resolve(config.dest, `${config.fontname}.${type}`)),
            getFileSize(String(content).length)
          )
        })
        resolve()
      })
    })
  }

  async watch () {
    this.init()
    let source = this.config.source
    let relativeSource = this.logger.escape(path.relative(process.cwd(), source))
    this.watcher = chokidar.watch(source)
    this.watcher
      .on('error', (error) => this.handleErrors(error))
      .on('ready', () => {
        this.info(`Watching {file:${relativeSource}}`)
        this.build()
        this.watcher.on('all', throttle(() => this.build(), 200))
      })
  }
}

module.exports = IconsBuilder
