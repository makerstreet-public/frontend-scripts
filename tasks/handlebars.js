const BuilderBase = require('../utils/builder-base')
const {appSource, appBuild} = require('../config/paths')
const {ensureArray} = require('../utils/helpers')
const {fileInfo} = require('../utils/stream')
const {fixChokidarPath} = require('../utils/helpers')

const vinylFs = require('vinyl-fs')
const path = require('path')
const rename = require('gulp-rename')
const handlebars = require('gulp-compile-handlebars')
const plumber = require('gulp-plumber')
const chokidar = require('chokidar')
const throttle = require('lodash.throttle')

const fs = require('fs')

class HandlebarsBuilder extends BuilderBase {

  init() {
    let config = this.config

    if (!config.source) {
      throw new Error('{var:config.source} must be defined')
    }

    if (!config.partials) {
      throw new Error('{var:config.partials} must be defined')
    }

    if (!config.watch) {
      throw new Error('{var:config.watch} must be defined')
    }

    if (!config.dest) {
      throw new Error('{var:config.dest} must be defined')
    }

    this.config = {
      ...config,
      source: path.resolve(appSource, config.source),
      batch: path.resolve(appSource, config.partials),
      watch: ensureArray(config.watch).map(entry => path.resolve(appSource, entry)),
      dest: path.resolve(appBuild, config.dest),
      siteData: config.siteData ? path.resolve(appSource, config.siteData) : null,
      helpers: config.helpers ? path.resolve(appSource, config.helpers) : null
    }

    this.init = () => {
    }
  }

  async build() {
    this.init();

    const {source, dest, batch} = this.config
    let {siteData, helpers} = this.config

    if (siteData) {
      siteData = JSON.parse(fs.readFileSync(siteData))
    }

    if (helpers) {
      helpers = require(helpers);
    }

    const options = {
      ignorePartials: false,
      batch: batch,
      helpers: helpers
    }

    return vinylFs.src(source)
        .pipe(plumber({
          errorHandler: (error) => {
            this.handleErrors(error)
            this.emit('end')
          }
        }))
        .pipe(handlebars(siteData, options))
        .pipe(rename(function (path) {
          path.extname = '.html'
        }))
        .pipe(vinylFs.dest(dest))
        .pipe(fileInfo(file => {
          this.info('   Done: {file:%s} [{size:%s}]',
              path.relative(process.cwd(), path.resolve(dest, file.name)),
              file.size
          )
        }))
  }

  async watch() {
    this.init()

    const {watch} = this.config

    let relativeSource = this.logger.escape(watch.map(entry => path.relative(process.cwd(), entry)).join(', '))
    this.watcher = chokidar.watch(fixChokidarPath(watch))
    this.watcher
        .on('error', (error) => this.handleErrors(error))
        .on('ready', () => {
          this.info(`Watching {file:${relativeSource}}`)
          this.build()
          this.watcher.on('all', throttle(() => this.build(), 200))
        })
  }
}

module.exports = HandlebarsBuilder;
