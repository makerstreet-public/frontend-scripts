const BuilderBase = require('../utils/builder-base');
const {appSource, appBuild} = require('../config/paths');
const {fileInfo, pipeIf} = require('../utils/stream')
const {fixChokidarPath} = require('../utils/helpers')

const vinylFs = require('vinyl-fs');
const path = require('path');
const rename = require('gulp-rename')
const plumber = require('gulp-plumber')
const chokidar = require('chokidar')
const throttle = require('lodash.throttle')


class CopyBuilder extends BuilderBase {

  init() {
    let config = this.config;

    if (!config.source) {
      throw new Error('{var:config.source} must be a glob to the main file')
    }
    if (!config.dest) {
      throw new Error('{var:config.dest} must be a output directory')
    }

    this.config = {
      ...config,
      source: path.resolve(appSource, config.source),
      dest: path.resolve(appBuild, config.dest)
    }
    this.init = () => {};
  }
  async build () {
    this.init()
    let config = this.config;

    this.info('Copying: {file:%s}', this.logger.escape(path.relative(process.cwd(), config.source)))
    return vinylFs.src(config.source)
      .pipe(plumber({
        errorHandler: function(error) {
          self.handleErrors(error)
          this.emit('end')
        }
      }))
      .pipe(pipeIf(!config.keepStructure, () => rename({dirname: ''})))
      .pipe(vinylFs.dest(config.dest))
      .pipe(fileInfo(file => {
        this.info('   Done: {file:%s} [{size:%s}]',
          path.relative(process.cwd(), path.resolve(config.dest, file.name)),
          file.size
        )
      }))
  }
  async watch() {
    this.init();
    let source = this.config.source
    let relativeSource = this.logger.escape(path.relative(process.cwd(), source))
    this.watcher = chokidar.watch(fixChokidarPath(source))
    this.watcher
      .on('error', (error) => this.handleErrors(error))
      .on('ready', () => {
        this.info(`Watching {file:${relativeSource}}`)
        this.build()
        this.watcher.on('all', throttle(() => this.build(), 200))
      })
  }
}



module.exports = CopyBuilder
